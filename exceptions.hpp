/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __EXCEPTIONS_HPP
#define __EXCEPTIONS_HPP
#include <fmt/format.h>
#include <string_view>
#include <stdexcept>



struct ParseError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
struct ConnectionError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
struct DesyncError : public std::exception {
    const char *what() const throw() {
        return "Server has desynced!!!";
    }
};

struct InsufficientArgsError : public ParseError {
    using ParseError::ParseError;

    InsufficientArgsError(std::string_view where, size_t expected, size_t given)
        : ParseError::ParseError(fmt::format("In {} event: Insufficient arguments expected. Given {} but expected {}", where, given, expected)) {}
};
#endif
