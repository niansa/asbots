/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _UTILITY_HPP
#define _UTILITY_HPP
#include <string>
#include <string_view>
#include <vector>
#include <tuple>



namespace Utility {
constexpr size_t strSplitInf = 0;

std::vector<std::string_view> strSplit(std::string_view s, char delimiter, size_t times = strSplitInf);
std::tuple<std::string_view, std::string_view> splitOnce(std::string_view s, std::string_view at);
void argsSizeCheck(std::string_view where, std::vector<std::string_view> args, size_t expected);
std::string lowers(std::string_view str);
}
#endif
