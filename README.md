# asbots
This project aims for a simple yet complete irc services implementation for TS6 IRC daemons. Not completed, but feel free to pick it up as a base for your own project!

## Building
This project uses CMake:

    git clone https://gitlab.com/niansa/asbots.git
    cd asbots
    mkdir build
    cd build
    cmake ..
    make -j$(nproc)

## Dependencies
The only direct library dependency is [libasync-uv](https://gitlab.com/niansa/libasync-uv). A C++20 compliant compiler is required. See its README for installation instructions.

## Supported platforms
This should in theory work on almost all platforms, however I can't offer support for Windows since I can't test the software there.
Feel free to offer some help in getting this to work on Windows.

## Config file
The config file uses the "INIL" (INI-like) format, parsed by [`configParser.cpp`](/configParser.cpp). It's syntax is:

    [section name]
    key: value
    end

An example is provided as [`config.inil`](/config.inil)
