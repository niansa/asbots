/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SERVICEBASE_HPP
#define _SERVICEBASE_HPP
class Event;
class User;
class Instance;

#include "uid.hpp"
#include "incompletes.hpp"

#include <async/result.hpp>
#include <string>
#include <string_view>
#include <unordered_map>
#include <functional>
#include <optional>



class ServiceBase {
public:
    UUID uuid;
    Instance *i;
    bool ready = false;
    std::unordered_map<std::string_view, std::tuple<std::function<async::result<void> (User *sender, const std::vector<std::string_view>& args)>, std::string_view, size_t, bool>> commands;

    virtual async::result<void> intitialize() = 0;
    virtual async::result<void> on_event(const Event& event);
    virtual async::result<void> on_direct_privmsg(std::string_view msg, User *author);

    async::result<void> mark_ready(const User& user);
    std::optional<std::string_view> getConfig(const std::string& section, const std::string& key);
};
#endif
