/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "serviceBase.hpp"
#include "instance.hpp"

#include <async/result.hpp>
#include <fmt/format.h>
#include <string>
#include <optional>
#include <stdexcept>

using fmt::operator""_format;



async::result<void> ServiceBase::mark_ready(const User& user) {
    co_await i->netInfo.wait_ready();
    co_await i->send_event(user.get_euid());
    ready = true;
}

std::optional<std::string_view> ServiceBase::getConfig(const std::string& section, const std::string& key) {
    auto &cfg = i->config.misc;
    // Get section
    auto s_res = cfg.find(section);
    if (s_res == cfg.end()) {
        return {};
    }
    // Get key
    auto k_res = s_res->second.find(key);
    if (k_res == s_res->second.end()) {
        return {};
    }
    // Return final result
    return {k_res->second};
}

async::result<void> ServiceBase::on_direct_privmsg(std::string_view msg, User *author) {
    auto split = Utility::strSplit(msg, ' ');
    User user = {.server = i->config.server.uid, .uid = uuid};

    auto res = commands.find(Utility::lowers(split[0]));
    if (res == commands.end()) {
        co_await i->send_event(user.get_notice("Error: Command not found", author->uid));
    } else {
        split.erase(split.begin());
        auto& [fnc, usage, minArgs, requiresAuth] = res->second;
        if (requiresAuth && !author->loginName.has_value()) {
            co_await i->send_event(user.get_notice("Error: You need to identify first", author->uid));
        } else if (split.size() < minArgs) {
            co_await i->send_event(user.get_notice("Usage: {}"_format(usage), author->uid));
        } else {
            co_await fnc(author, split);
        }
    }
}

async::result<void> ServiceBase::on_event(const Event& event) {
    co_return;
}
