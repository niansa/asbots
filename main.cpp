/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "config.hpp"
#include "instance.hpp"

#include "services/nickserv.hpp"
#include "services/chanserv.hpp"

#include <uvpp.hpp>
#include <async/result.hpp>
using namespace std;



int main() {
    using namespace uvpp;
    async::run_queue rq;
    async::queue_scope qs{&rq};
    loop_service service;

    Instance instance(service, parseConfig("config.inil"));
    instance.services.push_back(std::make_unique<NickServ>());
    instance.services.push_back(std::make_unique<ChanServ>());

    async::detach(instance.run());
    async::run_forever(rq.run_token(), loop_service_wrapper{service});
}
