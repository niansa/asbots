/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _CONFIG_HPP
#define _CONFIG_HPP
#include "uid.hpp"

#include <string>
#include <unordered_map>
#include <filesystem>
#include <stdexcept>



struct Config {
    struct ParseError : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    struct Connection {
        std::string addr;
        int port;
    } connection;
    struct Auth {
        std::string send_password, accept_password;
    } auth;
    struct Server {
        std::string name, description;
        SUID uid; // Fourth one reserved for null-terminator
        bool hidden = false;
    } server;

    std::unordered_map<std::string, std::unordered_map<std::string, std::string>> misc;
};


Config parseConfig(const std::filesystem::path& path);
#endif
