/*
 *  asbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "utility.hpp"
#include "uid.hpp"
#include "instance.hpp"
#include "exceptions.hpp"

#include <string>
#include <string_view>
#include <vector>
#include <tuple>



namespace Utility {
std::vector<std::string_view> strSplit(std::string_view s, char delimiter, size_t times) {
    std::vector<std::string_view> to_return;
    decltype(s.size()) start = 0, finish = 0;
    while ((finish = s.find_first_of(delimiter, start)) != std::string_view::npos) {
        to_return.emplace_back(s.substr(start, finish - start));
        start = finish + 1;
        if (to_return.size() == times) { break; }
    }
    to_return.emplace_back(s.substr(start));
    return to_return;
}

std::tuple<std::string_view, std::string_view> splitOnce(std::string_view s, std::string_view at) {
    // Find the colon
    auto colonPos = s.find(at);
    if (colonPos == s.npos) {
        return {s, ""};
    }
    // Split there
    return {s.substr(0, colonPos), s.substr(colonPos+2, s.size()-1)};
}

void argsSizeCheck(std::string_view where, std::vector<std::string_view> args, size_t expected) {
    if (args.size() < expected) {
        throw InsufficientArgsError(where, expected, args.size());
    }
}

std::string lowers(std::string_view str) {
    std::string fres(str);
    for (auto& character : fres) {
        if (isalpha(character)) {
            character = std::tolower(character);
        }
    }
    return fres;
}
}


static const char UUIDChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
UUID Instance::UUIDGen() {
    size_t numUUID = ++lastUUID;
    UUID fres = config.server.uid.str();
    for (auto it = fres.array.end() - 1; it != fres.array.begin()+SUID_len-1; it--) {
        auto idx = std::min(numUUID, sizeof(UUIDChars)-1);
        *it = UUIDChars[idx];
        if (idx != sizeof(UUIDGen())-1) {
            numUUID -= idx;
        }
    }
    return fres;
}
